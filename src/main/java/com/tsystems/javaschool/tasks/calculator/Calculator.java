package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    //list to help parse numeric values
    private static final List<String> NUMBER_CONSTITUENTS = new ArrayList<>(
            Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."));

    public String evaluate(String statement) {
        try {
            //tokenize input
            ArrayList<String> tokenizedStatement = tokenize(statement);

            //represent tokenized input in Reverse Polish Notation
            Queue<String> statementInRPN = convertToRPN(tokenizedStatement);

            //define stack of operations and operand variables
            Stack<String> operationStack = new Stack<>();
            double operand1;
            double operand2;

            //populate and evaluate stack of operations
            for (String token : statementInRPN) {
                switch (token) {
                    case "+":
                        operand2 = Double.parseDouble(operationStack.pop());
                        operand1 = Double.parseDouble(operationStack.pop());
                        operationStack.push(Double.toString(operand1 + operand2));
                        break;
                    case "-":
                        operand2 = Double.parseDouble(operationStack.pop());
                        operand1 = Double.parseDouble(operationStack.pop());
                        operationStack.push(Double.toString(operand1 - operand2));
                        break;
                    case "*":
                        operand2 = Double.parseDouble(operationStack.pop());
                        operand1 = Double.parseDouble(operationStack.pop());
                        operationStack.push(Double.toString(operand1 * operand2));
                        break;
                    case "/":
                        operand2 = Double.parseDouble(operationStack.pop());
                        if (operand2 == 0) {
                            throw new IllegalArgumentException();
                        }
                        operand1 = Double.parseDouble(operationStack.pop());
                        operationStack.push(Double.toString(operand1 / operand2));
                        break;
                    default:
                        operationStack.push(token);
                        break;
                }
            }


            String endResult = operationStack.pop();
            //adequately represent the result in case it is int
            if (Double.parseDouble(endResult) % 1 == 0) {
                endResult = endResult.substring(0, endResult.lastIndexOf('.'));
            }
            return endResult;
        }
        catch (Exception ex) {
            return null;
        }
    }
    private ArrayList<String> tokenize(String statement){
        ArrayList<String> result = new ArrayList<>();

        for(int i = 0; i < statement.length(); i++)
        {
            switch (statement.charAt(i)){
                case '(':
                case ')':
                case '+':
                case '/':
                case '*':
                case '-':
                    result.add(String.valueOf(statement.charAt(i)));
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    String readNumber = readNumberToken(statement.substring(i));
                    result.add(readNumber);
                    i += readNumber.length()-1;
                    break;
                default:
                    return null;
            }
        }
        return result;
    }

    private String readNumberToken(String statement){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i<statement.length(); i++){
            if(NUMBER_CONSTITUENTS.contains(String.valueOf(statement.charAt(i)))){
                stringBuilder.append(statement.charAt(i));
            }
            else{
                break;
            }
        }
        return stringBuilder.toString();
    }

    private Queue<String> convertToRPN(ArrayList<String> tokenizedStatement){
        //represent order of operations as Map
        Map<String, Integer> precedence = new HashMap<>();
        precedence.put("/", 2);
        precedence.put("*", 2);
        precedence.put("+", 1);
        precedence.put("-", 1);
        precedence.put("(", 0);

        //define queue to hold the result
        Queue<String> queue = new LinkedList<>();
        //define stack to hold the operators
        Stack<String> stack = new Stack<>();

        for(String token : tokenizedStatement){
            if("(".equals(token)){
                stack.push(token);
                continue;
            }

            if(")".equals(token)){
                while(!"(".equals(stack.peek())){
                    queue.add(stack.pop());
                }
                stack.pop();
                continue;
            }

            if(precedence.containsKey(token)){
                while(!stack.empty() && precedence.get(token) <= precedence.get(stack.peek())){
                    queue.add(stack.pop());
                }
                stack.push(token);
            }

            else{
                queue.add(token);
            }
        }

        while(!stack.isEmpty()){
            queue.add(stack.pop());
        }

        return queue;
    }
}
