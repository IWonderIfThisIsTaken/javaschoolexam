package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        validateInput(x, y);

        //remove duplicates from y
        Set<Object> distinctY = new LinkedHashSet<Object>(y);

        //remove elements that are not in x
        distinctY.retainAll(x);
        y = new ArrayList(distinctY);

        return x.equals(y);
    }

    private void validateInput(List x, List y){
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
    }
}
