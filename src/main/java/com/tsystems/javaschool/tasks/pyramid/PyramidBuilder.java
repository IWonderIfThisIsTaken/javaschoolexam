package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if(!isInputValid(inputNumbers)){
            throw new CannotBuildPyramidException();
        }

        //calculate the triangular number
        int n = (int)(Math.sqrt(8 * inputNumbers.size() + 1) - 1) / 2;

        //sort input List and assign to queue for easier use
        try {
            Collections.sort(inputNumbers);
        }catch (NullPointerException ex){
            throw new CannotBuildPyramidException();
        }
        LinkedList<Integer> queue = new LinkedList<>(inputNumbers);

        //create and populate the resulting array
        int[][] result = new int[n][2 * n - 1];

        //iterate over pyramid rows
        for(int i = 0; i < n; i++){
            //iterate over pyramid row elements
            for(int j = 0; j < 2 * n - 1; j++)
            {
                //check for pyramid edges
                if (j >= n - i - 1 && j <= n + i - 1){
                    //populate odd rows
                    if(i % 2 == 0){
                        //populate odd or even row elements, depending on n
                        if((n % 2 == 0 && j % 2 == 1) || (n % 2 == 1 && j % 2 == 0)){

                                result[i][j] = queue.poll();

                        }
                    }
                    //populate even rows
                    else if(i % 2 == 1){
                        //populate odd or even row elements, depending on n
                        if((n % 2 == 0 && j % 2 == 0) || (n % 2 == 1 && j % 2 == 1)){

                                result[i][j] = queue.poll();

                        }
                    }
                }
            }
        }
        return result;
    }

    private boolean isInputValid(List<Integer> input){
        return Math.sqrt(8 * input.size() + 1) % 1 == 0;
    }
}
